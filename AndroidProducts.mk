#
# Copyright (C) 2021 The Android Open Source Project
# Copyright (C) 2021 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/twrp_X00TD.mk

COMMON_LUNCH_CHOICES := \
    twrp_X00TD-user \
    twrp_X00TD-userdebug \
    twrp_X00TD-eng
